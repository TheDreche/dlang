// vim:filetype=none:noexpandtab:
//
// (()) is the replacement for previous compiler.std.Type
// ((.a: stdlib.Boolean)) is the type of all types where for every object of them the function .a is defined with the result as an stdlib.Boolean
(
	compiler.std.call: (type: (())) : (()),
	.fmap: (
		type_from: (()),
		type_to: (()),
		function: type_from: type_to,
	): {.}(type_from): {.}(type_to),
)
