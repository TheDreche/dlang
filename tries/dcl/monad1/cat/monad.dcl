// vim:filetype=none:noexpandtab:
compiler.std.combineTypes(
	lhs=(
		.join: (
			type: (()),
			merged: {.}({.}(type))
		) : {.}(type),
		.return: (type: (()), value: type): {.}(type),
	),
	rhs=functor,
)
