// vim:ft=none:

// Return types for functions are omitted in many cases

list = (ItemType: compiler.std.Type): compiler.std.Type {
	(
		size: Size,
		get: (id: Size, Size.compare(lhs=id, rhs=size).less): Item
	)
};

list_modifiers = (
	empty = (ItemType: compiler.std.Type) {
		list(ItemType = ItemType) (
			size = constants.size.0
			// get can't be called since no Size is less than 0
		)
	},
	prepend = {
		Type = compiler.std.Type;
		(ItemType: Type) {
			(original_list: list, item: ItemType) {
				list(ItemType)(
					size = Size.add(original_list.size, constants.size.1),
					get = (id: Size, Size.compare(lhs=id, rhs=size).less): ItemType {
						compiler.std.if(
							condition = Size.compare(lhs=id, rhs=constants.size.0).equal,
							then = item,
							else = original_list.get(id.previous())
						)
					}
				)
			}
		}
	},
	map = (ItemType: compiler.std.Type) {
		(original_list: list(ItemType), modification: (item: ItemType): ItemType) {
			list(ItemType)(
				size = original_list.size,
				get = (id: Size, Size.compare(lhs=id, rhs=size).less): ItemType {
					modification(item = original_list.get(id = id))
				}
			)
		}
	}
);

compiler.std.productTypeValueProduct(list, list_modifiers)
