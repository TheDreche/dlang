# Tries

This is a collection of trying to use the different languages for finding a good
syntax for everything.

This helps finding a good syntax for languages or testing whether the ideas are
enough for making a language able to do anything it should be able to do.

These tries mostly only have the interesting part and leave out the boring ones,
thus are not stand alone.
