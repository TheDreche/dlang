# Modules

Since I want DPL to be universal in the sense of that I don't assume a file
system (or something similar), a module system must not be bound to files.

Instead, one can view the whole code as a single value. But when doing that,
there is no encapsulation, as any value may refer to any other value.

One general question from this is: How can source code from different people be
combined? Does one really need to copy the whole code or is there an alternative
where one has a central repository of code?

When doing this, this central repository has the fundamental problem of naming.
Naming has to be done at the module level, as any less context aware level does
not know the names available and the more context aware don't need to.

Also at this level, one needs to do type checking.

Now, the question is: What is a perfect identifier?

Generally, the flexibility can be huge: One may want to name it in standard
English while someone else may prefer a picture as the name. Since details on
how to represent code belong into comments, these don't serve well as names.

Instead, one may try to make it representation dependent: An identifier is a
pointer to a value, whatever that may be in the concrete representation.

That leaves the problem of hiding identifiers from other values.

Another aspect are function parameters: There is an infinite amount of
possibilities to write any writable function, while still missing a lot of
functions. While this last aspect probably can't be solved with finite
resources, the first one can't be solved perfectly, but good enough by ignoring
it (and taking function equality for not automatically provable). But then it is
enough to have a way to introduce a function parameter for representing any
representable function; this function parameter is only accessible in the
function body.

And then it is known fact that in λ-calculus one can introduce variables by
simply adding a parameter with the value of the variable, so this reduces the
problem to elegantly representing values only accessible within limited contexts
(additionally: How do blackbox types act? (types whose definition si not
accessible from a function as the type has been given by parameter) and does one
need to define several values from the same expression? (probably not when only
having sum, product and function types, even if they are dependent)).
