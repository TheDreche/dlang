# Programming paradigms

## Action language

Define a bunch of starting conditions, ending conditions and a bunch of possible
actions (like functions) you can take; each has a set of preconditions and
postconditions (conditions which will change by doing that action).

In short:

- Design by contract
- No function definition here (done elsewhere in another language suitable for
  this)
- Starting state
- Goal state conditions

## Agent orientated

Specilization of OOP.

## Array

Mainly use arrays with a huge standard library. See APL.

## Automata-based

State machines. Repeat one switch statement many times.

## Concurrent computing

Threads.

## Data driven

Match data against some rules and modify it. Sed, awk, …

## Declarative

Mention what is a result and do not care about how to get to it.

### Functional

- Functions
- Pure functions
- Recursion

### Logic

- Define functions either by implementing them or adding some axioms
- A function definition essentially is a logic gate using values of other
  functions
- Only logical values (true/false)

### Constraint

- Define constraints for the goal
- Optional: Define functions for solving the constraints

#### Constraint logic

Like logic programming, but boolean values may be replaced with constraints.

### Dataflow

- Functions are like black boxes (actually in different languages)
- Functions run as soon as all of their arguments become valid
- Focus on connections between functions (dataflow/datastream)

#### Flow-based

No difference to regular dataflow programming?

#### Reactive

React to changes on values instantly:

```
b = 5
c = 3
// := : reactive assignment
a := b + c

b = 10
// Now, a = 13
```

### Ontology

- Describe a formal system

### Query language

- Query data

## Differentiable

- Differentiate functions
- Only numbers
- Allows gradient based optimizations

## Dynamic

Common:

- Eval
- Adding new types on runtime
- Dynamic typing
  - Otherwise usually type inference
- Variable memory allocation
- Reflective programming
- Find out details about type:
  - instanceof
  - type(...) = ...
  - List members of object
  - …

## Event driven

- Main loop listening for events
- Event types
- Callback functions
- Global mutable variables (how global?)

## Function-level

- variable free
- type hierarchy:
  - atoms
  - function (atom...) -> atom
  - higher order functions (function, function) -> function; no other way for
    generating new functions

### Tacit

- No named function parameters
- Functions are compositions of other functions

#### Concatenative

- All expressions are functions

```
x foo bar baz
```

means

```py
baz(bar(foo(x)))
```

## Generic

- Like C++ templates
- Concepts are like interfaces for them

## Imperative

Care about how to get to a result and less about what it is.

### Procedural

- Functions

### OOP

- Classes, Methods, …
- Class/Object/… relationships:
  - Has a (composition, attribute)
  - Is a (inheritance)

#### Multimorphic

- Multimorphism

## Intentional

- Write intention instead of code
- Add completely new abstractions

## Language-Oriented

- Languages are building blocks
- A new DSL for every part of the problem

### Domain specific

- Such languages are only used for a specific domain

## Literate

- Code is embedded into a description of what it does (inverse of comments)

## Natural language programming

- Write code in a common human language, e.g. English
- Each statement is represented by a sentence

## Metaprogramming

- Programs are regular data as well

Possibilities:

- Computations at compile time
- Self modifying programs

Terminology:

- Reflection: Programming language is its own Metalanguage
- Metaclass: Class whose instances are classes (“inherits from the class called
  class”)
- Homoiconicity: Programming language has data structures for representing its
  own code

### Automatic programming

- Higher abstraction level
- Automatically generate programs

#### Inductive

Solve problems usually written in a declarative style, logic or functional.

### Reflective

- Programs can modify themselves
- Get values by searching the more global environment for them (method =
  class.getMethod(name); method.call(object) instead of object.method())

### Attribute-oriented

- Embed metadata in source code

### Macro

Macros are called like that, because they replace a small sequence of characters
by a big one.

- Unsyntactic: Exactly like C macros.
- Syntactic: Work on ASTs instead of on lexical tokens.
- Hygienic: Do not expose internal symbols

### Template metaprogramming

- Like C++ templates
- Discovered by accident
- C++20 concepts match it

## Non-structured

- `goto`

### Array

-> above

## Nondeterministic

- Multiple branches
- If one branch fails, another is chosen

## Parallel

- Multiple threads

### Process orientated

- Separates data structures from acting upon them
