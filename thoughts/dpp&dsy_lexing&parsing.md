# DPP Lexing & Parsing

There should be a stack for the rules of lexing and parsing. When the topmost
grammar ends, the grammar and its corresponding lexer should be popped and
lexing/parsing continues as usual (from past the last token part of the
grammar).

## Lexing

When lexing, there have to be rules for separating tokens. Every character has
to be inside a token, but it may be ignored for parsing.

## Parsing

Like with a BNF syntax, but:

- Nodes can have additional attributes to be determined later
- Nodes are either compositions or decisions:
  - Compositions are like final classes
  - Decisions are like abstract classes
- Nodes may use a function

Example of a modified BNF syntax (it is based on dcl):

```
|Start| = <FunctionGenerated> {
	length: int;
}
<FunctionGenerated: |Start| > =
	<grammar: Grammar>
	(content: parseGrammar grammar)
{
	length: int = 5;
}
```

Would look like something as the following (with the actual parsing stuff left
out):

```
.Start = (length: int);
.FunctionGenerated = compiler.std.Type.combine(
	(
		.grammar: Grammar,
		.content: parseGrammar(grammar),
		.length: int = 5,
	),
	Start,
)
```
