# Reactive Programming

- Haskell implementation: rfpnow:
  https://web.archive.org/web/20150701030428/http://www.cse.chalmers.se/~atze/papers/prprfrp.pdf
- Lingua franca has reactors, they should be possible to implement by using
  functions instead in above implementation
