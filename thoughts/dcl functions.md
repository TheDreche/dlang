# DCL functions

There is a problem with the current design of dcl functions.

## Problem group #1

### Subproblems

#### Subproblem #1

Currently, one always defines a function with parameter names:

```
(param1: Type, param2: Type) {
	// …
}
```

However, when creating a class, one also creates several functions. What is the
function signature in the following example of `a.b`?:

```
(
	a.b: std.Int,
)
```

It definetely is something like `(...: (a.b: std.Int)): std.Int`, but what is
the `...`? What is the parameter name?

The old plan was to not have one and use the single parameter special function
call syntax instead, which *slightly* conflicts with the goal not to
differenciate between member functions and external functions.

#### Subproblem #2

Thus, there is another thing: There is a syntax for calling a function with an
argument without a name, but there is no way to declare such a function or to
mention its type.

#### Subproblem #3

It still is possible to return a function, meaning a function with multiple
unnamed parameters should be possible (when one with one unnamed parameter is
possible), but with the current syntax it looks ugly, even when not mentioning
the return type:

```
(p1: Type) {
	(p2: Type2) {
		p1+p2
	}
}
```

In other functional languages this usually looks better:

```.hs
func p1 p2 = p1+p2
```

```.nix
p1: p2: p1+p2
```

In these languages, however, no type can be mentioned for the parameters, they
don't really have a name and it doesn't look nice when there are too many
parameters and they need to be written onto a second line, if this is at all
possible.

#### Subproblem #4

This current concept of having names for the arguments forbids currying, which
seems to be only possible with this common approach to not have names for the
parameters.

### Thoughts on solution

#### Other languages

Other languages have similar problems.

##### Typescript

Typescript has a similar syntax for writing types of functions.

```.ts
let myAdd: (baseValue: number, increment: number) => number = function (
	x: number,
	y: number
): number {
	return x + y;
};
```

Thus, in here:

- Function parameters have a name
- That name is only for readability, but for nothing else (matches as long as
  order matches) and can't be used to identify parameters
- The function definition for a function returning a function looks as ugly as
  it does in dcl
- One uses an arrow `=>` instead of a colon `:` for the type, but not for the
  function return type

##### OCaml

OCaml allows to give
[names to parameters](https://ocamlbook.org/functions/#named-and-optional-arguments):

```.ml
let greet ~greeting ~name = Printf.printf "%s %s!\n" greeting name

let () = greet ~name:"world" ~greeting:"hello"
```

##### Nix

Nix has a consistent syntax for functions with multiple unnamed arguments:

```.nix
a: b: a+b
```

Also, it has the concept that there is only one value defined within each file,
like dcl should have.

#### Other thoughts

- Functions taking only one argument should be the default
- Functions with named parameters should be a syntactic sugar for creating an
  object type, giving each member the name of a parameter (within some
  namespace), mapping the members to local variables within the function, and
  when passing arguments creating an object of that type. This makes sure that
  the problem solution does not involve a thing like OCaml has where there is an
  order but that may be changed when having a named parameter.
- Functions with such a parameter may also be applied partially by not passing
  all names to this first parameter
- Function calls should have brackets/braces around them for making the
  indentation clear

With this, about everything can be kept, a new syntax is still needed for
functions with unnamed parameters, but it still ugly to write such functions (as
before).

### Steps in thoughts: Possible solutions

#### Possible solution #1

##### Description

This solution consists in adding a syntax for defining a function with
parameters without name by leaving out the name:

```
(: Type = self) {
	(: Type2 = other) {
		add(self)(other)
	}
}
```

##### Problems

This still has the problem that defining this function looks ugly as before. It
has fixed the problem of not having a way to define a function without parameter
name. Also, calling this function takes quite many lines when aligning braces:

```
f(
	value1
) ( // unnecessary line
	value2
)
```

##### Improvements

- Syntax for only needing one set of parentheses for defining and using a
  function with multiple unnamed parameters

#### Possible solution #2

##### Description

In difference to the previous solution, one can add a convenient syntax for
multiple arguments without names by adding a comma in between them, at
definition and usage.

```
f = (:Type = self, :Type2 = other) {
	add(self, other)
};
f(
	a,
	b, // trailing comma always optional
)
```

##### Problems

This is much better, but it has the problem of mixing named and unnamed
parameters still is cumbersome:

```
f = (:Type = self) {
	(a: Type2, b: Type3) {
		add(a, b)
	}
};
f(
	self
) ( // unnecessary line
	a = o1,
	b = o2,
)
```

Also, this is a side effect of using the syntax consistently, the syntax is not
the beauty one may want, but because the syntax is very consistent, this may be
ignored.

##### Improvements

- Add a syntax for merging named and unnamed parameter sections into one (with
  only one set of parentheses)

#### Possible solution #3

##### Description

One can freely mix named and unnamed parameters, where it is important to note
that the named parameter sets work, like intended, as a parameter with a special
type and local bindings. Thus, all parameters need to be in order, but the named
ones being of the same set may be in any order.

```
f = (:Type = self, a: Type2, b: Type3) {
	add(a, b)
};
f(self, a = o1, b = o2)
```

When it should be clearer that these are actually separate, the old syntax can
still be used:

```
f(self)(a = o1, b = o2)
```

##### Problems

- No way for doing the last point in function definitions

##### Improvements

- Provide other syntax for function definitions, resembling more the old style
  but not requiring the unnecessary curly braces

#### Possible solution #4

##### Description

Also provide a syntax for that by allowing a function definition to be the body
of a function without requiring the curly braces:

```
(: Type1 = self)(a: Type2, b: Type3) {
	add(a, b)
}
```

With all allowed type annotations, this would look like this:

```
(: Type1 = self): (a: Type2, b: Type3): ResultT
(a: Type2, b: Type3): ResultT {
	add(a, b)
}
```

##### Problems

- Inconsistencies between named and unnamed parameters:
  - One can define values for arbitrary unnamed parameters within the first set
    without a lambda but only for the first with unnamed ones instead

  - Things such as currying and reversing parameters can only be done for
    functions with unnamed parameters without compiler support.

    This point actually has a logical solution:

    - Reversing parameters makes only sense if one defines it as swapping names,
      but that doesn't make sense as a definition as one could ask why one can't
      pick completely new names
    - Currying could switch it for an actual class (Problem here: Not doable
      without compiler support)

    The problem here arises from the fact that there is a place where order has
    a meaning in the language: With functions, it is always one function that
    returns another function, where the function and the resulting function are
    not interchangeable.
- This gives syntactic sugar (something that could be expressed differently
  already) to a language whose aim is to be minimalistic for use with a
  preprocessor who could add all of that syntax.

##### Improvements

Not having named parameters in the main language, but having them implemented by
dpp instead?

### Final solution

- Do not have named parameters
- Named parameters are syntactic sugar to be implemented by preprocessors
- For consistency, because syntactic sugar is not really wanted and this is even
  closer to one, don't have that syntax for calling a function with multiple
  unnamed arguments with a single set of parentheses.

Because now, there are no named parameters anymore, the syntax can also change
to be simpler for unnamed parameters, by binding directly to a local variable:

```
f: (Type1): (Type2): (Type3): ResultT;
f = (self: Type1) {
	(a: Type2) {
		(b: Type3) {
			add(a)(b)
		}
	}
};
```

## Problem group #2

### Subproblems

#### Subproblem #1

The other common syntax is to call functions with whitespace. Because dcl
doesn't have named parameters anymore, this function call syntax would make more
sense (as it is clear that there only is one parameter).

This also means that the parentheses syntax is not needed anymore. However, this
would make the function definition syntax inconsistent, with the problem that
that can't be adjusted easily because the following could mean two things:

```
self {
	a
}
```

It could mean “Define a function with one parameter self” or “Call function self
with parameter a”.

#### Subproblem #2

This introduces the following ambiguity: Is a or b the result?

```
a = f {g h};
b = {f g} h;
f g h
```

### Thoughts on solution

#### Other languages

This time, the interesting part is always only how languages with that
whitespace function call syntax manage handle both subproblems.

##### Haskell

```.hs
-- Function definition with syntactic sugar:
call a b = a b
-- Without syntactic sugar:
call = \a -> \b -> a b

-- Type operator precedence (group right: RTL):
call :: (A -> B) -> A -> B
call :: (A -> B) -> (A -> B)

-- Call operator precedence (group left: LTR)
f = call a b
f = (call a) b
```

##### OCaml

```.ml
(* Definition with syntactic sugar *)
call a b = a b
(* Definition without syntactic sugar *)
call = fun a -> (fun b -> a b)

(* Type operator precedence: RTL like Haskell *)
(* Call operator precedence: LTR like Haskell *)
```

##### Nix

```.nix
let
	# Defintion
	call = a: b: a b;

	# Nix does not have type annotations
in
	# Call operator precedence: LTR like Haskell
	f: g: call f g
	# f: g: (call f) g
```

##### Lisp

Lisp has a syntax that has surprisingly many similarities:

```.lisp
;; Definition
(defun call (a b) "Doc" (a b))

;; Lisp does not have type annotations
;; Call operator precedence: Not required as unique because of parentheses
```

#### Other thoughts

- With this, the nix solution is the most consistent for the definition because
  it can be read from left to right without parentheses: Take a and give the
  function taking b and calling a with b
- This style of defining functions has the problem of missing type annotations
- This syntax in general has the problem of missing the information of the
  precedence, which, ideally, should not be needed to be noted explicitly by
  writing brackets as an example (which are not part of the syntax itself)
- According to this last point, the Lisp call syntax is the best, but there are
  a few problems as well:
  - When it is used, the definition syntax should also contain the round
    parentheses
  - It is made for having functions with any number of arguments (without
    restriction to one)
- For having the precedence clear, at least one of function or parameter needs
  brackets
- All of this actually is a well-known problem: Infix-operators can't be parsed
  uniquely: Because of this one needs something either in front or behind
  (currently this is a closing parentheses)
- Because the main reason for this idea was that the syntax may resemble that it
  is known that there is at most one parameter, another solution is to not allow
  commas inside function parentheses (technically, just not having a grammar
  rule covering this except when it's for types)

### Final solution

- Keep function call and definition syntax as they are:

  ```
  (a: Type1) {
  	(b: Type2) {
  		add(a)(b)
  	}
  }
  ```
