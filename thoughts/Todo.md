# TODO

This is a list of things to think about or take over.

- Default syntax for DPL (supporting optional inference of types (or parameters
  in general) and proofs)
- Programming language for specifying programming languages (how to specify
  syntax in general (thinking about C++), types, semantics)

## Add to Spec

- Namespace aliases, maybe?
- Stating theorems

## Add to stdlib

- Sum types (as function)
- Product types (as function)

## Required thoughts

- One order for evaluation

  - Left to right:
    - Natural (runs like read)
    - Definition has to be reversed (`value = constant`)
    - Declaration has to be reversed (`type: value` or similar)
    - Disadvantage: Constants won't be aligned properly
    - Better matches imperative style (how to get to a value is first, not the
      result)
  - Right to left:
    - Result first, details second; thus matches better to declarative
      programming

    - Result seen first thus (probably) better for debugging

    - Return value has to be first

    - Property access has to be reversed (`property<-constant` instead of
      `constant.property`)

    - Function calls have to be reversed (`(left=2, right=3)add<-Int`)

      Reason: One could also write `Int.add.call` if that meant the same, thus
      the call would be on the right either way; Now left (`call<-add<-Int`)

      One should choose good argument names for this to work well (probably, for
      add, the signature should be
      `Int<-(summand_left: Int, summand_right: Int)` because one can guess how
      `(summand_left = 5, summand_right = 7)` will continue from reading the
      word `summand`)

    - Autocompletion harder (one writes the left first; so one doesn't know
      where a result came from)

  Current is best because:

  - Meant for combining the well established concepts of imperative and
    declarative programming
  - Should not be too hard to debug (definition name left; doesn't create
    ambiguous code because statement)

## Solutions

- Identifier equality is not required for anything
- Values are equal when they can be interchanged freely without affecting the
  result (for proving stated theorems)
- Values of one type `t` can be used as values of another type `u` if and only
  if:
  - All core values of objects of type `u` also are defined for all objects of
    type `t`
  - All conditions for objects of type `u` also are fulfilled for all objects of
    type `t` (when conditions should be built in)
- Since there are no attributes as such, only functions, this comes close to
  right to left notation but leaves a little left to right (namespace access,
  arguments after function)
- There can, however, be namespace aliases
