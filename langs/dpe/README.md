# DPE

## Purpose

Programming language for syntactical extensions for dpl.

## Ultimate Goal

Programming languages tend to be much easier to write when having a simple
syntax for writing any kind of value. This is the point where most programming
languages differ.

For combining the best of every notation, DPL needs extensions for these. For
not specifying any part of the syntax, DPE aims to be a file type for
essentially what is syntax.

## Structure

### Given by DPL

- Scope rules (local identifier management: Top level extension has to give a
  scope of exported symbols)
- Definition of what an expression is (which requires defining which kinds of
  expressions exist)

### DPE Extension Defined

Interface:

- Decision node alternatives:
  - Tokenizer & lexer (which are only applied for that part)
  - Grammar
  - Conversion of AST to unified type defined by decision node
- Decision node types:
  - Name
  - Type

## Syntax

- Should not even allow manual source location management
- Keywords can be implemented by different lexer
- One should be able to define:
  - Tokenizer
  - Lexer
  - Parser
  - Semantical translator

Tokenizer:

- Has to have a notation for converting inner text into more convenient data
  types

Lexer:

- Shall not fail
- Should convert the remainder into usable data types
- Last stage that should get a predefined character data type?

Parser:

- May embed subextensions
- May contain extendable elements

Semantical translator:

- May need predefined standard library
- Has to source map every expression and
