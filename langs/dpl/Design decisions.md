# Design decisions

- Minimalistic
- Extensible
- Consistent
- No keywords
- Programming paradigm: Declarative (without time like in reactive programming)
- Prefer names over numbers and orders
- No redundance:
  - No need for include statements (but abbreviations are still possible)
  - No redundance by repeating name of value in file name and in constant name

Reason for no include statements: It's redundant to list everything external
that is used by the file at the top. For generating it, an external tool can be
used.

## Type system

- Structural typing (but with names determining the structure, not the order)
- Manifest or inferred (both should work)
- Static type system
- Types can be:
  - Functions (support running)
  - Sum types (support guaranteed deterministic and complete pattern matching)
  - Product types (support super-/subtyping and pattern matching with one
    deterministic complete pattern only)
  - Higher order types (like type classes)

## Supported types of polymorphism

- Inclusion (like inheritance)
- Parametric (like C++ templates)

## Case

The case language construct should support patterns for:

- Sum types
- Product types
- Expressions (may lead to nondeterministic results)

## Predefined constants

- Compiler things remain in `compiler` namespace, whose contents may change
  depending on the compilation target
