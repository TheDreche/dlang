# Origin of ideas

- Haskell:
  - Purely functional (declarative) paradigm:
    - No need to care about lifetime
  - Functions with no parameters are values
  - Lazy evaluation
  - Make preprocessor useless
- PROLOG:
  - Logic programming
- Curry:
  - Nondeterministic programming
  - Logic constraint programming
- C/C++:
  - Every statement ends in `;`
  - Line comments
  - Block comments
  - Aim of being minimalistic (from the comparison of both: C is easier)
  - Ignore whitespace at syntactical level
  - Blocks
  - Showing that compilers can do crazy optimizations (which can be even better
    with pure code)
- Rust:
  - Detect every undefined behavior at compile time (originally borrow checker)
    (but without too many false positives; for avoiding the halting problem the
    calculations of all values mentioned directly have to terminate)
- Java:
  - Dot notation `value.member`
  - No real need to `import` anything, but still allow abbreviations
  - Make directory structure important
- Python:
  - Have all libraries in global namespace
  - `name: type`
- Doxygen:
  - Special block comments
- A book about Java:
  - Numbers are actually constants (defined somewhere else)

Based on the above, the aims make up the remainder:

- Consistency:
  - Types and Functions are also being Values
  - Allow (unsigned) integers as unqualified identifiers (why an exception for
    them?)
  - Use `(...)` as type syntax: an object has a few values and may be usable as
    type for other objects
- Minimalistic but extensible:
  - No special syntax for templates
  - No standard library
  - No built-in types/functions/… except for target specific IO
  - Possibility to embed segments with custom syntax (aka. Haskell QuasiQuotes)
  - Custom syntactical extensions
