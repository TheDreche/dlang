# Structural type system

## What is structural typing?

Structural typing is the concept of stating that two types are compatible
(instances of the first can be used like instances of the second) when every
feature of the target type is also present in the source type.

A feature can be:

- A member value existing that has a specific type (the features of that type
  are considered like features of the type this member is a feature of, grouped
  by member name/position/…)
- A statement that specifies the connection between such members
- A constructor (especially for sum types)

When a type is compatible with another, the first is called a subtype and the
second a supertype.

Two types are the same if both are compatible with the other.

## Comparison to nominative typing

Advantages of structural typing:

- Easy handling of Ad-Hoc-types (but not impossible in nominative typing)
- Supertypes can always be created, even later on (nominative has the problem of
  needing a declaration for the conversion, usually done by a statement in the
  definition of the supertype that a structural equivalence shall be used)

Disadvantages of structural typing:

- Less type safety: If two types just happen to have the same structure, there
  will be not even a warning when converting between these
- It is not possible to really have a closed abstraction

## Trying to pick the best of both worlds

When taking structural typing as a base, the following things can be done:

- Allow exposing specific features of a newly defined type only to specific
  scopes. A similar thing is done in a lot of programming languages: Many
  object-oriented programming languages have `public` and `private`, which is
  essentially just this concept. Haskell, like many others, has the feature of
  not needing to hand every type constructor out of the current module.

  A similar concept can be added to structural typing: Every (product type)
  member is a feature, every statement about the members is a feature and every
  (sum type) constructor is a feature. It is also a special feature to have all
  other features of one kind of type; e.g. Having all sum type constructors
  allows pattern matching on it, having all product type members allows
  constructing it, …

  Haskell, in specific, has the feature of either exporting all features of a
  type by exporting all of its constructors, only specific constructors or
  exporting only members (with special record syntax for types with only one
  constructor avoiding the need of defining functions for every member using
  pattern matching on the single constructor). It does not allow exporting all
  constructors without guaranteeing that there are no others (as it allows
  pattern matching without having all possibilities covered as long as the
  result lies in a ̀`MonadFail`). Also, it does not allow only exporting the
  pattern matching feature of a constructor without the construction feature
  (with a workaround, one can define a type that/whose constructor is not
  exported and contains every member not to be retrievable by pattern matching).

  This fixes the second disadvantage.

- For fixing the first, it is important to be flexible in which features to
  expose to where. When being very flexible, like, limiting available features
  to every expression individually, the first disadvantage disappears as the
  expression doing bad stuff will not know about the features it needed to do
  its job incorrectly.

  The challenge now is to combine this with practical programming where it would
  be very inconvenient to specify which expression needs to know which features
  (which is actually only a type of redundancy as it is only redundancy that can
  be checked for correctness).

  In practical programming, it should be enough to have 2 types of visibility:
  Public and private (a lot of programming languages use this scheme, like C,
  Rust, Haskell, … - This can demonstrate that this works well). There has to be
  a small scope which should be convenient enough for defining everything that
  cannot be defined from the public features and small enough for not having the
  first problem.

## Features of

### Product types

- All members (with their names and features of their type) are features {allow
  doing stuff with the members}
- All statements about them are features (when meaningful, depending on what a
  type is)
- Having all members is a feature

### Sum types

Here, pure sum types (without embedded product type) are covered.

- All constructors are multiple features:
  - Construction of an instance of the sum type (cf. features of functions)
  - Detecting whether an instance was created using this constructor and
    extracting parameter (one can remove features by using a supertype as this
    parameter type)
- Having all constructors (knowing they're constructors; when one does not want
  to expose they are constructors, one can replace a constructor by 2 functions:
  `Param -> T` and `(Param -> r) -> T -> Maybe r`) (allows not needing a
  fallback for pattern matching)

### Functions

Functions are special: It makes more sense to talk about what features of `a`
and `b` in `a -> b` add and which remove features of the function.

- Adding a feature to `a` removes a feature of `a -> b`
- Adding a feature to `b` adds a feature of `a -> b`
