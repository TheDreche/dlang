# DPL

## Terminology

- Object: Set of statements to be fulfilled (that may be empty)
- Statement: Like an object; fulfilled when the statement object is solvable
- Property: Variable in an object
- Solution: A set of property values fulfilling an object
- Value: ? (Also define equivalence)
- Nothing: Object with no solution
- Functional object: Object with exactly one solution (all others are
  equivalent)
- Solvable object: Object with at least one solution
- Nondeterministic object: Object with multiple solutions

## Expression

### Expression Kinds

- Switch case (includes property access)
- Object (and): A bunch of expressions using properties
- Object (or): A bunch of constructors with objects
- Object (induction): Exponential type
- Object (equivalence): Equivalence type

### Usages

- Values may be used as statement: When there is a set of properties fulfilling
  all statements, the statement is considered to be true
- Switch case

### Patterns

Patterns can have 2 forms:

- And part: Extract properties
- Sum type: Match constructors

In case the input value is nondeterministic, all possible solutions have to be
tried. The result is trying everything non-deterministically (from the other
context, it may be deducible that some values can't be the result and get
eliminated, as always with nondeterministic values).

For the and part, there may only be a single pattern. This leads to only having
one case that matches.

## Proofs

### Provable Properties

- Entry point is a functional value (and only produces functional monad
  continuations)
- No value can be of a subtype of a sum and a product type (for checking whether
  any of multiple conditions is true, one should add a condition like
  `{} => (L a | R b);`); every switch case statement states whether its value is
  sum or product; mixing is an error
