# Abstract Data Types

Abstract data types have the advantage of not specifying implementation details
at all. Instead, one only specifies a set of possible states (which can be
inferred as the free object), some operations and some axioms about the type,
which are the same when taking type theory into account. The only thing that has
to be checked is that there are not any contradictions (like being able to
instantiate an instance which leads to a false statement, according to an axiom,
being true).

## Product type

Generally, a product type can be done using one constructor and one function
returning the result of a given function taking all constructor parameters
(which is continuation passing style for having one function per constructor
parameter extracting them).

## Sum type

A sum type can be represented as having one constructor function per sum type
constructor and a function acting like a `case` expression in other languages.

## Example

For example, a stack can be defined using the following operations and axioms
(pseudocode):

```
empty : T -> Stack T,
push : T: Type -> (Stack T, T) -> Stack(T),
pop : T: Type -> Stack T -> Maybe (Stack T, T),
popPush : T: Type -> s: Stack T -> i: T -> pop T (push T (s, i)) = Just (s, i),
popEmpty : T: Type -> pop T (empty T) = Nothing,
```

In this case, the compiler can infer the structure as follows:

1. `empty`, `push` and `pop` are the only functions returning a stack.
2. `pop` is completely defined from the axioms, so it doesn't need an extra sum
   type constructor.
3. `empty` cannot be equal to any `push (s, i)` because then `pop` needed to be
   both `Nothing` (by `popEmpty`) and `Just (s, i)` (by `popPush`).
4. 2 sum type constructors: `empty` and `push`, taking their respective
   parameters.
5. `pop = \case Empty -> Nothing; Push (s, i) -> Just (s, i)`
