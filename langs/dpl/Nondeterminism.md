# Nondeterminism

## Referential transparency

When nondeterminism is built directly into expressions, this breaks referential
transparency. For example, imagine having `f(x) = (a == a)`. Then, there are 2
strategies for evaluating `f(1 ? 2)`:

- First picking any of `1` or `2` and then passing the result into `f`,
  resulting in either `f(1)` or `f(2)`, both being true
- First substituting `1 ? 2` into `f`, resulting in `(1 ? 2) == (1 ? 2)`, giving
  four possible expressions: `1 == 1`, `1 == 2`, `2 == 1`, `2 == 2`, resulting
  in `f(1 ? 2) == (True ? False)`

## Breaking nondeterminism on assignment

One may have the idea to take the choice exactly when assigning values to names.
The above case would then resolve into the first evaluation, as the value is
bound to a name with the function evaluation, binding it to the parameter name.

## Nondeterminism as monad

One may have noticed that the semantics seem to resemble statements and thus
this slightly resembling the set monad: `a ? b` is the set containing `a` and
`b`, and the breakage of referential transparency arises from how many times
this is being run: `(1 ? 2) >>= return . f` will result in `True`, while
`f (1 ? 2)` (for `f n = n >>= \lhs -> n >>= \rhs -> return $ lhs == rhs`) will
result in `True ? False`.

This has some other interesting properties as well:

- Since set is also a monoid (empty set as neutral parameter for union
  biendofunctor in the category of sets), an incomplete function can be
  represented as returning a singleton set on success and empty set on
  undefined. This in turn leads to being able to exclude nondeterministic IO
  completely (by not allowing sets of IOs), and execution paths which cannot be
  taken can be filled by a proof instead (since the type of the proof is a
  contradiction to the execution path being taken, one can proof it and then
  call it with data from the execution path to get the bottom type, which will
  result in the type system allowing this path as it may choose to cast into the
  appropriate type, thus having the compiler check the proof without any special
  support.) Functions for escaping this strong system may be given for debugging
  purposes or for the case when it would be difficult to proof, so for similar
  reasons than Haskell ̀`unsafePerformIO`.
- One may decide to have a builtin not specifying the proof but automatically
  trying to proof/disproof the unreachability of the execution path.
- A convenience function can be added for taking a single value of this set. The
  result is unspecified for sets with multiple elements, however (it still has
  to be within the set).
- This nondeterministic set doesn't need to have a finite amount of items. For
  any type, there has to be a set containing all possible values (for working
  for all types).
- Nondeterminism is explicit, so functions are aware of whether their parameters
  may be nondeterministic.
- However, data representations may be confusing as they should be done without
  this monad (the value should rather be nondeterministic in the function, not
  in the type), even if it is certain which constructor will be chosen when only
  having one (thus, this being deterministic in a value which contains
  nondeterministic parts and thus is completely embedded in this
  nondeterministic monad).
