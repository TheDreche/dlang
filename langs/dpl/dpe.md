# DPL & DPE

## Interface

### Expression

- Local identifier
- Property (of current local scope)
- Property access (of different local scope)
- Pattern matching

### Statements

Every statement is like an equation. Pattern matching is allowed here as well.

### Pattern matching

### Global scopes

- Identifiers:
  - Extension specific prefix OR `dpl`
  - Identifier kind (e.g. normal, operator, …): Also specifies what to save
    along with the identifier (e.g. operator precedence)
  - Name
