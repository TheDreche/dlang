# Syntax

The syntax is given by `dpp` files; but since they will (probably) need a turing
complete programming language to implement another syntax, there has to be a
starting point.

This file specifies an example syntax that can be used as a starting point.

## Modules

TODO

## Expressions

```
top = {};
bottom = ();
true = top;
false = bottom;
not = {input.statement => bottom};
Unit = top;
Empty = bottom;
undefined = bottom;
Boolean = (True {} | False {});

Pair = {
  FST = input.Fst;
  SND = input.Snd;
  fst = input.fst;
  snd = input.snd;
  {} => {FST; fst};
  {} => {SND; snd};
};

fst = {
  input.param (
    {input.fst = first}: {input.result = first};
  );
};
snd = {
  input.param (
    {input.snd = second}: {input.result = second};
  );
};

Function = {
  Param = input.Param;
  param = input.param;
  Result = input.Result;
  result = input.result;
  {} => {Param; param};
  {} => {Result; result};
};

compose = {
  lhs = input.lhs;
  rhs = input.rhs;
  param = input.param;
  result = input.result;

  {Function; lhs} => {Function; rhs} => {Function; result};

  result = {lhs; input.param = param} {
    {input.result = tmp}: {rhs; input.param = tmp} {
      {input.result = r}: {r=result};
    };
  };
};

// Associativity (only adds a hint for the compiler for a property to check)
{Function; 1}
=> {Function; 2}
=> {Function; 3}
=> {compose;
  input.lhs = 1;
  input.rhs = {compose;
    input.lhs = 2;
    input.rhs = 3;
  } {
    {input.result = r}: r
  };
} {
  {input.result = r}: r;
} = {compose;
  input.lhs = {compose;
    input.lhs = 1;
    input.rhs = 2;
  } {
    {input.result = r}: r;
  };
  input.rhs = 3;
} {
  {input.result = r}: r;
};
```
