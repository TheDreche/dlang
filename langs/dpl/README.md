# dpl

## Purpose

General purpose declarative programming language.

## Ultimate goal

The ultimate goal is to have a complete system for all kinds of declarative
development.

Additionally, as many checks as possible should be doable automatically, like,
if possible, checking whether the computation for the IO monad is deterministic
(it may be constructed using nondeterministic functions, though). However, it is
very normal that not everything can be checked, as a lot of things would require
solving the halting problem. Ideally hints can be added making checks easier
(like a complete hint, meaning the function can never be undefined, making it
possible to check other functions with this assumption of the given function and
checking the function itself for correctness; this could be done by kind-of unit
tests not needed to be written even; a minimum checks may be trying to find
inputs to contradict this hint).

## Programming paradigms

Generally: Declarative

- Functional
- Logic
- Nondeterministic
- Reactive

Additionally:

- Function level
- Value level

Because of the goal to be minimalistic it is better not to need to define a
function type. When having the functional programming paradigm built-in,
however, this needs to be done. For avoiding this, functional programming should
be defined using the other paradigms (which is possible). The same applies for
function and value level programming.

## Structure

Syntax:

- Completely given by extensions
- Type for top level: Scope
  - Scope: Map between names and positioned expressions
  - Expression:
    - Subscope
    - Function call
    - Typed (TODO: Necessary or replaceable by hints?)
    - Value (and type) construction
    - Hinted (hints are for compiler specific checks)
    - Case

Types:

- Manifest or inferred both work
- Structural

Identifiers/import system:

- Syntax given by extensions
- Multiple local values each file

## Type system

- Static
- Structural:
  - Product type members are not identified by position but by name
  - Sum types: TODO

Allowing:

- Dependent types
- Intersections (TODO: whether there is an intersection type for any two types
  is still an open question)
- Refinements

TODO: Add types (for not having the problem of `()` being an inhabitant of the
`()` type).

TODO: Think about when to allow mixing abstraction levels and when not.

## What are types and functions?

Functions and types are the same:

- Exported members (traditionally attributes or parameters and return type,
  whose last two are the same in logic programming)
- A set of equations that has to hold for them (traditionally refinements or the
  function body)

## What are values?
