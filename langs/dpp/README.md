# dpp

## Purpose

Powerful preprocessing for any file type.

## End goal

Having a complete and powerful alternative preprocessor that is an alternative
to using the C preprocessor for everything (with the complicated m4 as
alternative).

## State

Current ideas:

- Preprocessor instructions have to be marked (like in the C preprocessor and
  unlike m4)
- Marking a comment will result in it being ignored
- Files should read like dpl code with unmarked text being a syntax extension
- Unmarked text may be interpolated
