# dlang

Dreches computer language collection

## Language overview

- dpl: General purpose declarative language
- dll: Language for defining low level data structures to be used with dpl
- dpe: DPL extension (Actually, I may rather want a binary format for dpl and
  some tools to create/edit it, like a transpiler)
- dpp: Preprocessor language (anything but my focus)

Look into their subdirectories in `langs` for more information on them.

## Removed

### DCFG

Configuration of individual programs fits well into a system configuration. This
system configuration may also contain the packages being installed. And the
result of this is the Nix (or Guix) package managers. So, there is no need for
another domain specific language (they can just generate configuration in any
file format if required).

### DCL & DIL

Both have been merged into DPL (the language I really want to develop by
thinking about combining all kinds of declarative programming with features
useful for programming).

The idea is that an application can be interpreted or compiled with the same
code. (This may be helpful at times.)

### DML

The name incorrectly suggests it could be a ml dialect and really, this language
only exists for testing implementation of a functional language which is not
really to be used.

### DUI

Functional reactive programming should be implementable within dpl, and the
domain specific language for specifying user interfaces should be library
specific. If it turns out to be impossible to integrate functional reactive
programming into dpl, a replacement will return.

### DSY

Mal-defined goals (What is a computer language? Is there one defined output
format?)

dpe has well-defined goals and should provide about the same features (some are
still missing; e.g. the programming language type system is given).
